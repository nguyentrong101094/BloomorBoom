﻿## BLOOM OR BOOM
Lập trình: Nguyễn Lê Hồng Trọng

Engine: GameMaker Studio Pro 1.4

Ngôn ngữ: GML

# Mô tả:
Bloom or Boom là một mobile game thuộc thể loại Shoot'em Up & Bullet Hell (bắn máy bay và đạn dày đặc màn hình, VD: DoDonPaChi, Touhou).

Người chơi có thể chọn 1 trong 2 nhân vật Lửa hoặc Băng. Mỗi nhân vật có sức mạnh và kỹ năng riêng, người chơi có thể tùy chỉnh và nâng cấp. Tùy theo cách nâng cấp mà cách chơi có thể thay đổi rất nhiều.

# Link download:
- PC: [Google Drive](https://drive.google.com/open?id=1_2tj3LhdM7fCBJngZUJ9K9iCD1hurkWP).
- Android: [Google Drive](https://drive.google.com/open?id=1TDR1nDKWx0B67JNCzFra8bfUxjTbenla).

---
# Cách chơi:
Người chơi có thể chọn độ khó ở màn hình chính bằng cách xê dịch nút hiển thị độ khó (mặc định là Normal).

Endless Mode: sống sót càng lâu càng tốt, cứ một thời gian sẽ xuất hiện Boss.

Stage Mode: chơi theo bàn. Mỗi bàn có một mục tiêu riêng:
- Bàn 1: sống sót, khi thời gian hết, bạn sẽ nhận được sao và mở khóa bàn tiếp theo.
- Bàn 2,3,5,6: hạ gục boss. Hạ boss càng nhanh càng được nhiều sao.
- Bàn 4: tiêu diệt đủ số lượng kẻ địch để qua bàn.

**Powerup:**
Thỉnh thoảng trên màn hình sẽ xuất hiện hạt năng lượng màu xanh. Thu thập chúng để tích tụ năng lượng.

Khi đủ năng lượng, nhân vật lửa sẽ tự động kích hoạt sức mạnh của mình. Nhân vật lửa trở nên bất khả xâm phạm (trừ ngoại lệ lúc đánh boss Nucleus, những hạt nhỏ có thể làm hại bạn kể cả lúc này)

Nhân vật băng có thể để dành để tấn công khi kích hoạt.

**Nâng cấp:**
Ở màn hình chính, vào Character / Custom để nâng cấp nhân vật.

Nâng cấp các kỹ năng cần có sao, để thu thập sao, bạn có thể chơi qua bàn hoặc thu thập điểm số để lấy EXP lên level.

Bấm ? (chuyển sang màu đỏ) rồi bấm vào các kỹ năng để xem thông tin về kỹ năng.

Bấm REDO để hoàn trả lại toàn bộ sao.

---
**Điều khiển:**

Chế độ:
- Chính xác: bấm chuột/ngón tay vào đâu, nhân vật chạy đến đó, có thanh trượt để điều chỉnh khoảng cách.
- Theo cử động: di chuột/ngón tay như thế nào, nhân vật sẽ di chuyển theo, chế độ này thuận lợi hơn cho chơi trên di động.

**PC:** Điều khiển bằng cách bấm giữ chuột vào màn hình.
Sử dụng năng lượng: 
- Nhân vật Lửa: tự động kích hoạt
- Nhân vật Băng: khi đủ năng lượng, bấm Space để kích hoạt. Hoặc bấm giữ chuột phải vào nơi muốn tấn công (khi sử dụng Hàn lực tập trung)

**Điện thoại:** Điều khiển bằng ngón tay.
Sử dụng năng lượng: 
- Nhân vật Băng: khi đủ năng lượng, bấm vào biểu tượng ở góc phải dưới để kích hoạt. Hoặc bấm giữ ngón tay thứ 2 (cần ấn 2 ngón lên màn hình cùng lúc) vào nơi muốn tấn công (khi sử dụng Hàn lực tập trung)

---
## Cheat:
- Bấm vào góc trái trên cùng màn hình để xin boss
- Bấm M trong game để kích hoạt debug mode
- Trong debug mode: 
- Bấm backspace để kích hoạt bất tử
- Bấm nút 2 numpad để tua timeline
- Bấm nút 5 để sản sinh hạt năng lượng