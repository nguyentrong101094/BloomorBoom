/***************************************************
  destroy all object of keys or obj type to clear memory
 ***************************************************/
trace("spawning pool destroy",typeof(argument0));

var key = argument0;
var obj_des;
obj_des = SpawnPool_FindValue(key);

if (!is_undefined(obj_des)){
    instance_destroy(obj_des,false);
}

var cachedList = ds_map_find_value(global.SpawnPool.CacheMap, key);
if (cachedList == undefined){
    trace("SpawnPool_DestroyObj: undefined cachelist");
} else {
    ds_list_destroy(cachedList);
}
