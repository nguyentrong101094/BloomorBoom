///rotate_smooth(destination_angle, current_angle, rotation_speed, speed_limit)
var destination_angle, current_angle, angleDifference;
var destination_angle = argument0;
var current_angle = argument1;
angleDifference= destination_angle-current_angle;

if ((angleDifference >= 180) && (current_angle < 180) )
{
     current_angle += 360;
}
else 
if (angleDifference <= -180)
{
     current_angle -= 360;
     return(current_angle);
}

//Apply rotation speed limit, or none if it's set to -1
if (argument3 == -1)
{
     current_angle += angleDifference / argument2;
}
else
{
     if (angleDifference / argument2 < argument3)
     {
          current_angle += angleDifference / argument2;
     }
     else
     {
          current_angle += argument3 * sign( angleDifference );
     }
}
return(current_angle);
