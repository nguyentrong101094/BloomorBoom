#define Game_SetPowerRate
with (obj_Parent_timeline) {
    if (argument0 == "reset") {
        powerRate = powerRateDefault;
    } else {
        powerRate = argument0;
    }
}

#define Game_SetNextPowerSpawn
with(obj_Parent_timeline) {
    if !global.boss_exist
        alarm[0] = powerRate * random_range(10,15);
    else alarm[0]=room_speed*random_range(7,10);
}

#define Game_SetDefaultPowerRate
with(obj_Parent_timeline) {
    powerRateDefault = argument0;
    powerRate = powerRateDefault;
}