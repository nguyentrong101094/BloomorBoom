//0easy, 1medium, 2hard
last_timeline=timeline_index;
timeline_pick=timeline_index;
while timeline_pick=last_timeline { //ensure no repeat timeline
switch round(random(argument0)) {
    case 0:        
        switch round(random(2)){
            case 0: timeline_pick=global.TL_e[0]; break;
            case 1: timeline_pick=global.TL_e[1]; break;
            case 2: timeline_pick=global.TL_e[2]; break;
        }
        break;
    case 1:
        switch round(random(2)){
            case 0: timeline_pick=global.TL_m[0]; break;
            case 1: timeline_pick=global.TL_m[1]; break;
            case 2: timeline_pick=global.TL_m[2]; break;
        }
        break;
    case 2:
        switch round(random(2)){
            case 0: timeline_pick=global.TL_h[0]; break;
            case 1: timeline_pick=global.TL_h[1]; break;
            case 2: timeline_pick=global.TL_h[2]; break;
        }
        break;
}
}
timeline_index=timeline_pick;

//get timeline name to display for testing purpose)
global.timelineName=timeline_get_name(timeline_pick);

timeline_position = 0;
timeline_running = true;
timeline_loop = false;
