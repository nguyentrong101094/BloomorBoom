//create_confirm_dialog(x,y,"text","cancel text","ok text")
//if user click OK, the object calling this script will execute its "ev_other, ev_user0"
if instance_exists(obj_confirm_dialog) return noone;

var dialog = instance_create(argument[0],argument[1],obj_confirm_dialog);
dialog.dialog_parent = id; //which object ordered to create this dialog?
with (dialog) {
text = argument[2];
if argument_count >= 4 {
    text1 = argument[3];
    text2 = argument[4];
    button1.text=text1;
    button2.text = text2;
}
}
return dialog;
