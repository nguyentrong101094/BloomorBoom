/***************************************************
  func(obj)
  find key of object in spawning map
 ***************************************************/
var FindValue = argument0;
var size = ds_list_size(global.SpawnPool.SpawningMapKeyList);
var tempkey;
var returnObj;
for (i = 0; i < size; i++;)
   {
        //trace("SpawnPool_FindKey");
        tempkey = ds_list_find_value(global.SpawnPool.SpawningMapKeyList,i);
        returnObj = SpawnPool_FindValue(tempkey);
        //trace(tempkey,returnObj);
        if (returnObj == FindValue) {
            return (tempkey);
        }
   }
return(false);
