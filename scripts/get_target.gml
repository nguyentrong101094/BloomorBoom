#define get_target
//target detection
var target;
if !global.boss_exist {
    if instance_exists(obj_Parent_enemy)
        target = instance_nearest(x,y,obj_Parent_enemy);
    else return noone;
}
else {
    if instance_exists(obj_Parent_boss) target = obj_Parent_boss;
    else return noone;
}

return target;

#define homing_at
var target=noone,max_dirspd = 2,min_dirspd=1;
target=argument[0];
min_dirspd = argument[1];
max_dirspd=argument[2];
//homing
    dir=point_direction(x,y,target.x,target.y);
        direction-=sign(angle_difference(direction,dir))*dirspd;
        if abs(angle_difference(direction,dir))>20{    
        //if bullet miss the target, decrease speed
            if dirspd < max_dirspd dirspd +=0.01;
            //if spd>5 spd-=0.05;
            }
        else {
            if dirspd > min_dirspd dirspd -= 0.1;
            //if spd<maxSpd spd+=0.1;
        }

#define init_homing
seek=1;
target=noone;
dirspd=1;
