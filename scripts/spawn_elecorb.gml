/*
argument0: hor, vert (horizon, vertical)
argument1: top, bottom
argument2: left, right, center (if horizon)
*/
var xx,yy;
if argument0="random" argument0=choose("hor","vert");
if argument2="random" argument2=choose("left","center","right");
switch (argument0)
{
    case "hor": 
        if argument1="random" argument1=choose("top","bottom");
        switch (argument1){
            case "top": yy=yv-100; break;
            case "bottom": yy=view_height+100; break;
            default: yy=view_height+100;
        } 
        switch (argument2){
            case "left": xx=xv+30; break;
            case "center": xx=xv+135; break;
            case "right": xx=view_width-300; break;
        }
        break;
    case "vert":
        if argument1="random" argument1=choose("top","middle","bottom");
        switch (argument1){
            case "top": yy=yv+30;break;
            case "middle": yy=yv+345;break;
            case "bottom": yy=view_height-300; break;
            default: yy=view_height-300;
        } 
        switch (argument2){
            case "left": xx=xv-100; break;
            case "right": xx=view_width+100; break;
            default: xx=choose(xv-100,view_width+100);
        }
        break;
}
instance_create(xx,yy,obj_elec_orb);
