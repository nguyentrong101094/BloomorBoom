#define Particle_Init
/***************************************************
  func(enum_Particles.type,color)
  init particle designed in event_user 0 in obj_ParticleInit
 ***************************************************/
var partReturn = undefined;
with (global.ParticleManager) {
    var particleInit = argument[0];
    switch (argument_count){
        case 1: 
            partReturn = Particle_InitType(particleInit);
            break;
        case 2: 
            var color = argument[1];
            partReturn = Particle_InitTypeParam(particleInit,color);
            break;
    }
}
trace("Particle_Init",partReturn);
return (partReturn);

#define Particle_InitType
///Boss Death
var particleInit = argument0;
var partReturn = undefined;

if (ds_map_exists(Particle_DSMap,particleInit)) {
    return Particle_DSMap[? particleInit];
}

switch (particleInit){
    case enum_Particles.bossDeath:
        partReturn = Particle_InitTypeBossDeath();
        break;
    case enum_Particles.starSpark:
        var partStar = part_type_create();
        part_type_sprite(partStar,spr_star,0,0,0);
        part_type_size(partStar,0.60,0.80,-0.01,0);
        part_type_scale(partStar,1,1);
        part_type_alpha1(partStar,0.50);
        part_type_speed(partStar,4,5,0,0);
        part_type_direction(partStar,0,359,0,0);
        part_type_gravity(partStar,0.10,270);
        part_type_orientation(partStar,0,359,0,0,0);
        part_type_blend(partStar,1);
        part_type_life(partStar,60,60);
        partReturn = partStar;
        break;
}
ds_map_add(Particle_DSMap,particleInit,partReturn);
return partReturn;

#define Particle_InitTypeParam
var particleInit = argument0;
var color = argument1;
var partReturn = undefined;
switch (particleInit){
    /***************************************************
      Fire Spark
     ***************************************************/
    case enum_Particles.fireSpark:
        var str = string(color)+"FireSpark";
        if (ds_map_exists(Particle_DSMap,str)){
            return Particle_DSMap[? str];}
        var partFireHitSpark = part_type_create();
        part_type_shape(partFireHitSpark,pt_shape_flare);
        part_type_size(partFireHitSpark,1,2,0,0);
        part_type_scale(partFireHitSpark,1,1);
        part_type_color1(partFireHitSpark,color);
        part_type_alpha3(partFireHitSpark,0.7,0.6,0);
        part_type_speed(partFireHitSpark,3,7,0,0);
        part_type_direction(partFireHitSpark,30,150,0,0);
        part_type_gravity(partFireHitSpark,0.20,270);
        part_type_orientation(partFireHitSpark,0,0,0,0,1);
        part_type_blend(partFireHitSpark,1);
        part_type_life(partFireHitSpark,60,60);
        
        ds_map_add(Particle_DSMap,str,partFireHitSpark);
        partReturn = partFireHitSpark;
        break;
    /***************************************************
      Flare Explode >>>
     ***************************************************/
    case enum_Particles.flareExplode:
        var str = string(color)+"FlareExplode";
        if (ds_map_exists(Particle_DSMap,str)){
            return Particle_DSMap[? str];}
        var partFlareExplode = part_type_create();
        part_type_shape(partFlareExplode,pt_shape_flare);
        part_type_size(partFlareExplode,0.80,0.80,0,0);
        part_type_scale(partFlareExplode,3,1);
        part_type_color1(partFlareExplode,color);
        part_type_alpha3(partFlareExplode,1,1,0);
        part_type_speed(partFlareExplode,9,9,0,0);
        part_type_direction(partFlareExplode,0,359,0,0);
        part_type_gravity(partFlareExplode,0,270);
        part_type_orientation(partFlareExplode,0,0,0,0,1);
        part_type_blend(partFlareExplode,1);
        part_type_life(partFlareExplode,60,60);
        
        trace("Particle_InitTypeParam: color ",str);
        ds_map_add(Particle_DSMap,str,partFlareExplode);
        partReturn = partFlareExplode;
        break;
}

return partReturn;

#define Particle_InitTypeBossDeath
//if (!ds_map_exists(Particle_DSMap,enum_Particles.bossDeath)) {
    partBossDeathFrag = part_type_create();
    part_type_shape(partBossDeathFrag,pt_shape_sphere);
    part_type_size(partBossDeathFrag,2,2,0,0);
    part_type_scale(partBossDeathFrag,1,1);
    part_type_color1(partBossDeathFrag,16762415);
    part_type_alpha1(partBossDeathFrag,0.50);
    part_type_speed(partBossDeathFrag,3,3,-0.04,0);
    part_type_direction(partBossDeathFrag,0,359,0,20);
    part_type_gravity(partBossDeathFrag,0,270);
    part_type_orientation(partBossDeathFrag,0,60,0,0,1);
    part_type_blend(partBossDeathFrag,1);
    part_type_life(partBossDeathFrag,50,70);
    
    partBossDeathSpark = part_type_create();
    part_type_shape(partBossDeathSpark,pt_shape_line);
    part_type_size(partBossDeathSpark,0.20,0.40,0,0);
    part_type_scale(partBossDeathSpark,1,1.50);
    part_type_color3(partBossDeathSpark,5629695,3363583,7733473);
    part_type_alpha3(partBossDeathSpark,0.80,0.49,0.04);
    part_type_speed(partBossDeathSpark,5,9,0,0);
    part_type_direction(partBossDeathSpark,0,359,0,0);
    part_type_gravity(partBossDeathSpark,0.10,270);
    part_type_orientation(partBossDeathSpark,0,0,0,0,1);
    part_type_blend(partBossDeathSpark,1);
    part_type_life(partBossDeathSpark,90,120);
    
    part_type_death(partBossDeathFrag,30,partBossDeathSpark);
    ds_map_add(Particle_DSMap,enum_Particles.bossDeath,partBossDeathFrag);
    return (partBossDeathFrag);
//}
//return Particle_DSMap[? enum_Particles.bossDeath];

#define Particle_InitTypeFlareExplode
