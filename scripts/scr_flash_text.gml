//x,y,"text",[time,font,scale]
var obj=noone, xx=argument[0], yy=argument[1], text=argument[2];

if (!instance_exists(obj_flash_text))
    obj=instance_create(xx,yy,obj_flash_text);
else {
    obj = obj_flash_text;
    with (obj)
        event_perform(ev_create,0);
}
obj.text=text;
if (argument_count > 3) {
    var time = argument[3];
    var font = argument[4];
    var scale = argument[5];
    
    obj.time=time;
    obj.font=font;
    obj.scale=scale;
    
    with(obj) alarm[0]=time;
}
