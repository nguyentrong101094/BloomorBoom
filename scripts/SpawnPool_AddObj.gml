/***************************************************
SpawnPool_AddObj(key, object)
  add a key and its object to pool
 ***************************************************/

var Id = argument0;
var value = argument1;

//global.SpawningList;
//trace("spawn pool:",global.SpawnPool);
//trace("debug:",global.SpawnPool.object_index);
//if (!ds_map_exists(global.SpawnPool.SpawningMap, Id)) {
    ds_map_add(global.SpawnPool.SpawningMap, Id,value);

if (ds_list_find_index(global.SpawnPool.SpawningMapKeyList,Id)==-1)
    ds_list_add(global.SpawnPool.SpawningMapKeyList,Id);
