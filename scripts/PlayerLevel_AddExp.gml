#define PlayerLevel_AddExp

ID = global.charID;
var exp_gain = argument[0];
global.charEXP[ID] += exp_gain;

global.islevelup = false;
if global.charLV[ID] < 20 {
    exp_left = Character_EXPNeedForNextLV();
    while exp_left <= 0 {
        exp_gain -= exp_left;
        global.islevelup = true;
        global.charLV[ID] += 1;
        global.charEXP[ID] = max(0,exp_gain);
        exp_left = Character_EXPNeedForNextLV();
    }
    ini_write_real("player",global.charname[ID]+"LV",global.charLV[ID])
}


#define PlayerLevel_SaveExp
ini_write_real("player",global.charname[ID]+"EXP",global.charEXP[ID]) //save data