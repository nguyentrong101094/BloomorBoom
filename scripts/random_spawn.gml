/*random_spawn(x,obj)
x:  0: spawn at top
    1: spawn at sides & top
    2: spawn even at bottom
obj: bullet
xsl means start from left, xsr means right
yst means top, ysb mean bottom
*/
random_x=random_range(-40,view_wview+40);
if argument0>2 argument0=2
switch irandom(argument0){
    case 0: //set spawn at top
        xs=view_xview + random_x;
        ys=global.yst;
        break;
    case 1: //set spawn at upper half side
        xs=choose(global.xsl,global.xsr);
        ys=view_yview+random_range(-40,view_hview*0.7);
        break;
    case 2: //set spawn at bottom and lower half side
        switch round(random(1)){
            case 0:
                xs=view_xview + random_x;
                ys=global.ysb;
                break;
            case 1:
                xs=choose(global.xsl,global.xsr);
                ys=view_yview+random_range(-40,view_hview+40);
                break;                
        }
        break;
    default: 
        xs=choose(global.xsl,global.xsr);
        ys=view_yview+random_range(-40,view_hview/2);
        break;
}

instance_create(xs,ys,argument1);
