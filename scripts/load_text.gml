/*load_text(section,key,default)
if string not exists in ini, return argument default*/
var section,key,def;
section = argument0;
key = argument1;
def = argument2;
ini_open(Language);
def=ini_read_string(section,key,def);
ini_close();
return(def);
