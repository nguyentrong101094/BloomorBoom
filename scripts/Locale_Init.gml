#define Locale_Init
/*load_text(section,key,default)
if string not exists in ini, return argument default*/
ini_open(Language);

#define Locale_LoadText
/***************************************************
  func(section, panel, key text)
 ***************************************************/

var section,key,def;
section = argument0;
key = argument1;
def = argument2;

def=ini_read_string(section,key,def);

return(def);

#define Locale_Done
ini_close();