#define EnemyDeath_Event
var particleType = argument[0];
var paramParticle = argument[1];
var particleNum = argument[2];
var boom = argument[3];
if hp<=0 {
    global.enemy_killed++;
    var smboom = instance_create(x,y,obj_small_boom_effect);
    smboom.image_blend = explode_color;
    var ode = instance_create(x,y,obj_MinionDeath);
    with (ode) {
        EnemyDeath_Init(particleType,paramParticle,particleNum,boom);
    }
    var i
    for(i=0; i<11; i+=1){
        with (instance_create(x,y,obj_shard)) {
           direction = i * 36 + random(5);
        }
    }
    audio_play_sound(snd_Explosio_Diode,10,false);
}

#define EnemyDeath_Init
/***************************************************
  func(particle type, param particle, particle num, boom)
 ***************************************************/
var particleType = argument[0];
var paramParticle = argument[1];
var particleNum = argument[2];
var boom = argument[3];
var particleDeath = "";

if (paramParticle != false){
    particleDeath = Particle_Init(particleType,paramParticle);
} else {
    particleDeath = Particle_Init(particleType);
}
trace ("EnemyDeath_Init",boom);
Particle_QuickBurst(particleDeath,particleNum);
if (typeof(boom)=="number" && boom != false)
{instance_create(x,y,boom);}

instance_destroy();