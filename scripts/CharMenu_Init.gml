#define CharMenu_Init
switch(global.charID) {
    case enum_Characters.fireBoy: checkedBox = in_radioBox1; break;
    case enum_Characters.iceDude: checkedBox = in_radioBox2; break;
}

with (checkedBox) {
    CharMenu_TickRadioBox();
}

#define CharMenu_TickRadioBox
with (o_radioBox) {
    checked = false;
}
checked = true;
global.character = m_Character;
global.charID = m_charID;
ini_open("data.ini");
ini_write_string("pref","character",object_get_name(global.character));
ini_close();