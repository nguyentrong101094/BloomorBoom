/*
+ 1 difficult every 30 secs, 1 minute + 2 difficult
2 minute +4
example: difficult = 10 (hardest), dif_scale(5) will return 2
            difficult = 5 (medium), dif_rscale(5) return 1
*/
dif_i=global.difficult/argument0;
return(dif_i);
