/***************************************************
  funct(key/obj)
  create a new instance if not exist in pool, otherwise
  pull from pool
 ***************************************************/
 
var key = argument0;
var objIndex = SpawnPool_FindValue(key);
var cachedList = SpawnPool_GetCreateCacheList(key);
/*var cachedList = ds_map_find_value(global.SpawnPool.CacheMap, objIndex);
if (is_undefined(cachedList)){
    trace("SpawnPool_CreateFromCache: cache list not exist, creating new ",key);
    cachedList = ds_list_create();
    ds_map_add_list(global.SpawnPool.CacheMap,objIndex,cachedList);
}*/
var pooledObj;
if (ds_list_empty(cachedList)){
    pooledObj = instance_create(x,y,objIndex);
    //ds_list_add(cachedList, pooledObj);
} else {
    pooledObj = ds_list_find_value(cachedList,0);
    if (pooledObj == undefined) {
        trace("SpawnPool_CreateFromCache: pooledObj in cached list not found, possible reason: unexpected destroyed");
    }
    instance_activate_object(pooledObj);
    ds_list_delete(cachedList,0);
}
//    global.SpawnPool.CacheMap[? objIndex] = cachedList;
//ds_list_destroy(cachedList);
return(pooledObj);

