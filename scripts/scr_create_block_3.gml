//script create background falling block 3 sides
xx=choose(random(100),random_range(190,350),random_range(440,room_width));
if (xx<140 || xx>400) {
    if random(1)<0.7
        instance_create(xx,yv-200,obj_bg_block_side);
    else instance_create(xx,yv-200,obj_bg_block_side_far);
}
else {
    if random(1)<0.7
        instance_create(xx,yv-200,obj_bg_block);
    else instance_create(xx,yv-200,obj_bg_block_far);
}
//alarm[3]=room_speed*2;
