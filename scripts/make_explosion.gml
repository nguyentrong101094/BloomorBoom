//make_explosion(color)
var boom = instance_create(x,y,obj_small_boom_effect);
    boom.image_blend = argument0;
    var i
    for(i=0; i<11; i+=1){
        with (instance_create(x,y,obj_shard)) {
           direction = i * 36 + random(5);
        }
    }
    audio_play_sound(snd_Explosio_Diode,10,false);
