draw_set_alpha(image_alpha);
draw_set_halign(fa_center);
draw_set_valign(fa_middle);
draw_set_font(fnt_franko_m);
draw_set_colour(c_white);
draw_text_transformed(x,y-70,modeText,image_xscale,image_yscale,image_angle);
//draw_set_font(fnt_van_17);
draw_text_ext_transformed(x,y,describeText,50,450,image_xscale*0.7,image_yscale*0.7,image_angle);
