/***************************************************
  func([gameObject])
  return object to cache list, deactive
 ***************************************************/
 
var p_objToReturn;
switch (argument_count) {
    case 0: p_objToReturn = id; break;
    case 1: p_objToReturn = argument[0]; break;
}
var key = SpawnPool_FindKey(p_objToReturn.object_index);
if (key == false) {
    trace("SpawnPool_ReturnToCache: Error: cache list of key doesn't exists ",p_objToReturn,", object index: ",p_objToReturn.object_index);
    return false;
}
var cachedList = SpawnPool_GetCreateCacheList(key);
if (cachedList == undefined) {
    trace("SpawnPool_ReturnToCache: error: cachelist doesnt exist, destroying object");
    instance_destroy(p_objToReturn);
}
else {ds_list_add(cachedList,p_objToReturn);
    instance_deactivate_object(p_objToReturn);
}
