#define Event_Win
{
        if instance_exists(obj_player)
            with (obj_player) event_perform(ev_other,ev_user1);
        win=true;
        }

#define Event_VictoryPopup
instance_create(0,0,obj_victory_msg);
var ctbt = instance_create(270,600,obj_continue_button);
if (argument_count == 1) {
    ctbt.next_room = argument[0];
}