/* 
spawn_grid(percent x, percent y, obj)
argument0: spawn in 1 of 4 following place in screen
___
0 1|
2_3|
switch (argument0){
    case 0: i=1; j=1;break;
    case 1: i=2; j=1;break;
    case 2: i=1; j=2;break;
    case 3: i=2; j=2;break;
    default: i=2; j=2;break;
}
*/

instance_create(room_width*argument0,room_height*argument1,argument2);
