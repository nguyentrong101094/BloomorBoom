#define UI_SetCurrentLayer
Controller.m_UICurrentLayer = argument0;

#define UI_GetCurrentLayer
return Controller.m_UICurrentLayer

#define UI_AddLayer
UI_SetCurrentLayer(UI_GetCurrentLayer()+1)

#define UI_RemoveLayer
UI_SetCurrentLayer(UI_GetCurrentLayer()-1)