/***************************************************
  funct(key/obj, number)
  create a shit ton of instances and add it to pool
  useful when create from cache sometimes fails when it doesn't have
  enough instances
 ***************************************************/
 
var key = argument0;
var objIndex = SpawnPool_FindValue(key);
var cachedList = SpawnPool_GetCreateCacheList(key);
for (var i=0;i<number;i++){
    var pooledObj = instance_create(-10000,-10000,objIndex);
    ds_list_add(cachedList, pooledObj);
    instance_deactivate_object(pooledObj);
}

