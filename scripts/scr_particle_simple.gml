//Below is particle code:
color1=argument0;
//color2=make_colour_rgb(242,118,51);

particle1 = part_type_create();
part_type_shape(particle1,pt_shape_square);
part_type_size(particle1,0.1,0.15,0,0);
part_type_scale(particle1,1,1);
part_type_color1(particle1,color1);
part_type_alpha2(particle1,0.8,0.2);
part_type_speed(particle1,0.1,0.15,0,0);
part_type_direction(particle1,0,359,0,0);
part_type_gravity(particle1,0,270);
part_type_orientation(particle1,0,359,5,0,1);
part_type_blend(particle1,0);
part_type_life(particle1,30,50);

Sname=part_system_create();
part_system_depth(Sname,depth+10);
emitter1 = part_emitter_create(Sname);
//part_emitter_region(Sname,emitter1,x,x,y,y,0,0);
//part_emitter_burst(Sname,emitter1,particle1,2);
