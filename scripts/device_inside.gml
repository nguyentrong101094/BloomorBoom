/*
check if device number x is inside rectangle
device_check_inside(device num, x1,x2,y1,y2)
*/
var n=argument[0],x1=argument[1],x2=argument[2],y1=argument[3],y2=argument[4];
var mx = device_mouse_x(n), my = device_mouse_y(n);

return (mx > x1 && my > y1 && mx < x2 && my < y2);
