/***************************************************
  func(particle type, param particle, particle num, boom)
 ***************************************************/
var particleType = argument[0];
var paramParticle = argument[1];
var particleNum = argument[2];
var boom = argument[3];
var particleDeath = "";

if (paramParticle != false){
    particleDeath = Particle_Init(particleType,paramParticle);
} else {
    particleDeath = Particle_Init(particleType);
}
trace ("EnemyDeath_Init",particleDeath);
Particle_QuickBurst(particleDeath,particleNum);
if (boom != false) {instance_create(x,y,boom);}

instance_destroy();
