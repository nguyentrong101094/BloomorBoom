/*
rain_bullet(number of bullets,bullet-1,bullet-2...)
*/
var bm = instance_create(0,0,o_bulletmaker);
for (i=1; i<argument_count; i+=1)
{
    bm.bullet[i] = argument[i];
};
bm.num = argument[0];
bm.inum = argument_count;
bm.interval = room_speed * (1.5-dif_rscale_limit(5,1));
bm.origin = dif_scale(4);
