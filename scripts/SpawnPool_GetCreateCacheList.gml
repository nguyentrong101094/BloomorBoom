/***************************************************
  func(key)
  get (or create if not exists) cache list of instances of key. 
 ***************************************************/
var key = argument0;
var objIndex = SpawnPool_FindValue(key);
//if object index exists
if (!is_undefined(objIndex)){
    var cachedList = ds_map_find_value(global.SpawnPool.CacheMap, key);
    if (is_undefined(cachedList)){
        trace("SpawnPool_GetCreateCacheList: cache list not exist, creating new ",key);
        cachedList = ds_list_create();
        ds_map_add_list(global.SpawnPool.CacheMap, key, cachedList);
    }
    return (cachedList);
} else {
    trace("SpawnPool_GetCreateCacheList: object not exist in map, create fail",obj,"key:",key);
    return(false);
    }
