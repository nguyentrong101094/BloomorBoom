///add damage toward var dmg_received of target
/*
for debug purpose
dmg_calc_to(source damage, target)
*/
if instance_exists(obj_debug)
{
var atk = argument[0], target=argument[1];
    if (instance_exists(target)) {
        target.dmg_received += atk * (1- target.def);
        target.dmg_t=0;
    }
}
