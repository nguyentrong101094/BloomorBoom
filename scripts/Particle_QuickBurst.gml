/***************************************************
  func(part_type, part number)
 ***************************************************/
var temp_emitter = part_emitter_create(global.partS2);
part_emitter_region(global.partS2,temp_emitter,x,x,y,y,0,0);
part_emitter_burst(global.partS2,temp_emitter,argument0,argument1);
part_emitter_destroy(global.partS2,temp_emitter);
trace("Particle_QuickBurst: done",object_get_name(object_index));
