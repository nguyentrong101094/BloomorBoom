/***************************************************
  func(enum_Particles.type)
 ***************************************************/
if (!is_undefined(global.ParticleManager.Particle_DSMap[? argument[0]])) {
    return (global.ParticleManager.Particle_DSMap[? argument[0]]);   
}
else {
    trace("Particle_GetPartType error: particle not defined ",argument[0],", object: ",object_get_name(object_index));
    return false;
}
