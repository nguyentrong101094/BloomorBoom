/***************************************************
  SpawnPoolFindValue(key)
  get obj of key
  return type: number
 ***************************************************/
var key = argument0;
var obj_des;
switch (typeof(key)){
    case "number": obj_des = key; break;
    case "string": obj_des = ds_map_find_value(global.SpawnPool.SpawningMap,key); break;
}
return(obj_des);
