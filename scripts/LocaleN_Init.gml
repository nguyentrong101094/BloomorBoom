#define LocaleN_Init


#define LocaleN_Load
with (global.LanguageManager) {
    var key = argument0;
    var Default = argument1;
    if (ds_map_exists(m_LocaleMap, key)) {
        return (m_LocaleMap[? key]);
    }
    else return Default;
}