//create_dummy_player(x,y,which character)
var dummy = instance_create(argument0,argument1,obj_dummy_animate);
dummy.a_character = argument2
with (dummy)
    event_perform(ev_other,ev_user0);
    
return (dummy.id); //return id
